/* Creates Oracle structures, needs to be excuted into Strands Personal Finance db with the user SYSADMIN */
SET DEFINE ON
SET ECHO ON
SET VERIFY OFF

DEFINE adm_usr=&1
DEFINE adm_pwd=&2
DEFINE app_usr=&3
DEFINE app_pwd=&4
DEFINE app_role=&5
DEFINE tbs_name=&8
DEFINE tbs_datafile=&9

-- Create TABLESPACES:
CREATE TABLESPACE &tbs_name LOGGING DATAFILE '&tbs_datafile' SIZE 5G AUTOEXTEND ON NEXT 1G MAXSIZE 20G EXTENT MANAGEMENT LOCAL;

-- Create USERS (schemas)
CREATE USER &adm_usr IDENTIFIED BY &adm_pwd DEFAULT TABLESPACE &tbs_name QUOTA UNLIMITED ON &tbs_name;
CREATE USER &app_usr IDENTIFIED BY &app_pwd DEFAULT TABLESPACE &tbs_name QUOTA UNLIMITED ON &tbs_name;

-- Create ROLES
CREATE ROLE &app_role;

-- SYS grants to adm_usr
GRANT CREATE SESSION TO &adm_usr;
GRANT CREATE PROCEDURE TO &adm_usr;
GRANT CREATE SEQUENCE TO &adm_usr;
GRANT CREATE TABLE TO &adm_usr;
GRANT CREATE TRIGGER TO &adm_usr;
GRANT CREATE TYPE TO &adm_usr;
GRANT CREATE VIEW TO &adm_usr;
GRANT CREATE MATERIALIZED VIEW TO &adm_usr;

-- SYS grants to app_usr
GRANT CREATE SESSION TO &app_usr;
GRANT CREATE TABLE TO &app_usr;

-- Grant roles
GRANT &app_role TO &adm_usr;
GRANT &app_role TO &app_usr;

EXIT;
