whenever sqlerror exit 1

set echo on
set define off
set timing on
set verify off

------------------------------------------------------------------------------------------------------------------------
-- LOG_MODULE_MANAGMENT
------------------------------------------------------------------------------------------------------------------------
  CREATE TABLE LOG_MODULE_MANAGEMENT 
   (	LOG_SEQ NUMBER, 
	    LOG_DATE DATE, 
	    LOG_TYPE VARCHAR2(10 CHAR), 
	    LOG_TEXT VARCHAR2(4000 CHAR)
   ) NOLOGGING TABLESPACE ADMIN ;

  CREATE INDEX IDX_LOG_MODULE_DATE ON LOG_MODULE_MANAGEMENT (LOG_DATE) 
  TABLESPACE ADMIN ;

COMMENT ON TABLE LOG_MODULE_MANAGEMENT IS 'PKG_MODULE_MANAGMENT logs changes into this table';

CREATE SEQUENCE SEQ_LOG_MODULE_MANAGEMENT INCREMENT BY 1 START WITH 1 NOMAXVALUE MINVALUE 1 CACHE 20;

------------------------------------------------------------------------------------------------------------------------
-- CFG_OBJECT_TYPE_PRIVS
------------------------------------------------------------------------------------------------------------------------

 CREATE TABLE cfg_object_type_privs
   (	object_type VARCHAR2(30 BYTE) NOT NULL ENABLE,
	    privilege VARCHAR2(30 BYTE) NOT NULL ENABLE,
	    role VARCHAR2(2 BYTE) NOT NULL ENABLE,
	    CONSTRAINT CFG_OBJ_TYPE_PRIVS_ROLE_CHK CHECK (role in ('RO','RW')) ENABLE
   ) TABLESPACE ADMIN ;

   COMMENT ON COLUMN cfg_object_type_privs.object_type IS 'Object type';
   COMMENT ON COLUMN cfg_object_type_privs.privilege IS 'Privilege to grant';
   COMMENT ON COLUMN cfg_object_type_privs.role IS 'RO ReadOnly. RW ReadWrite';
   COMMENT ON TABLE cfg_object_type_privs  IS 'Configure the privileges to grant for each object type and access role';

------------------------------------------------------------------------------------------------------------------------
-- ADM_USER_PRIVS
------------------------------------------------------------------------------------------------------------------------

  CREATE TABLE adm_user_privs
   (	grantee VARCHAR2(30 CHAR) NOT NULL ENABLE,
	    owner VARCHAR2(30 CHAR) NOT NULL ENABLE,
	    role VARCHAR2(2 CHAR) NOT NULL ENABLE,
	    position NUMBER(2,0) NOT NULL ENABLE,
	    grantable VARCHAR2(3 CHAR) DEFAULT 'N',
	    CONSTRAINT role_chk CHECK (role in ('RO','RW')) ENABLE,
	    CONSTRAINT grantable_chk CHECK (grantable in ('Y','N')) ENABLE,
	    CONSTRAINT adm_user_privs_pk PRIMARY KEY (GRANTEE, OWNER)
   ) TABLESPACE ADMIN ;

   COMMENT ON COLUMN adm_user_privs.grantee IS 'User to whom access was granted';
   COMMENT ON COLUMN adm_user_privs.owner IS 'Owner of the object';
   COMMENT ON COLUMN adm_user_privs.role IS 'RO ReadOnly. RW ReadWrite';
   COMMENT ON COLUMN adm_user_privs.position IS 'Order to process user in order to solve dependences';
   COMMENT ON COLUMN adm_user_privs.grantable IS 'Privilege is grantable: Y/N';
   COMMENT ON TABLE adm_user_privs  IS 'Manage the schemas accessible by an user';



exit