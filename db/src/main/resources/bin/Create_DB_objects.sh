#!/bin/bash
########################################################################################################################
# Create_DB_objects.sh
#
# Date      Vers    Description
# --------- ------- ---------------------------------------------------------------------------------------------------
# 20200220  1.0     1st Version
########################################################################################################################

########################################################################################################################
# Program Variables
########################################################################################################################
HOST=$(hostname | tr '[:lower:]' '[:upper:]')
PROGRAM=$(basename "$0" .sh)

# Obtain PATHs Dynamically
APPL_DIR_TEMP=$(dirname "$0")                                               # Script path APPL_DIR_TEMP
CALL_PATH=$(pwd)                                                            # Executing from CALLPATH
if [[ "${APPL_DIR_TEMP}" != /* ]]; then                                     # Define application dir
    APPL_DIR=${CALL_PATH%%/bin}
else
    APPL_DIR=${APPL_DIR_TEMP%%/bin}
fi

APP_BIN_DIR=${APPL_DIR}/bin
APP_CFG_DIR=${APPL_DIR}/cfg
APP_LOG_DIR=${APPL_DIR}/log
APP_SQL_DIR=${APPL_DIR}/sql

. "${APP_CFG_DIR}"/environment                                              # Load environment file

if [ ! -z "${ORACLE_PDB}" ]; then                                               # If PDB informed, use it as SID
    export ORACLE_SID="${ORACLE_PDB}"
fi

########################################################################################################################
# LOG Functions
########################################################################################################################
init_log() {
    CURRDATE=$(date '+%Y%m%d')
    CURRTIME=$(date '+%H%M%S')
    LOGFILE="${APP_LOG_DIR}"/"${CURRDATE}"_"${CURRTIME}"_"${PROGRAM}".log
}

tty -s && log() {                                                               # if connected to a tty output the log messages otherwise just store them
    echo -e "$*" | tee -a "$LOGFILE"
} ||
log() {
    echo -e "$*" >> "$LOGFILE"
}

########################################################################################################################
# EXEC SQL Function
########################################################################################################################
exec_sql() {
    EXECDATE=$(date '+%Y%m%d')
    EXECTIME=$(date '+%H%M%S')
    FILENAME="$1"
    PARAMS="$2"
    SQLFILE="${APP_SQL_DIR}"/"${FILENAME}".sql
    SQLLOGFILE="${APP_LOG_DIR}"/"${EXECDATE}"_"${EXECTIME}"_"${FILENAME}".sql.log

    if [ ! -f "${SQLFILE}" ]; then                                              # Check that file exists
        log "${COL_FG_RED}File not found: ${COL_RESET} [ ${SQLFILE} ]"
        log "${COL_BOX_RED}ERROR${COL_RESET} ${COL_FG_RED}Process ${PROGRAM} cancelled due to errors!${COL_RESET}"
        exit 1;
    fi

    if [ "$LOG_VERBOSE" -ne 0 ]; then
        log "${COL_BOX_PURPLE}VERBOSE${COL_RESET} => Executing SQL File: [ ${SQLFILE} ]"
        log "${COL_BOX_PURPLE}VERBOSE${COL_RESET} => Log File: [ ${SQLLOGFILE} ]"
    fi

    sqlplus "${ADM_USR}"/"${ADM_PWD}"@"${ORACLE_SID}" @"${SQLFILE}" ${PARAMS} > "${SQLLOGFILE}"
    OUTPUT_CODE=$?
    errnum=$(grep -cE "ORA-[0-9]{5}|SP2-[0-9]{4}|PLS-[0-9]{5}" "${SQLLOGFILE}")

    if [ "$OUTPUT_CODE" -ne 0 ] || [ "$errnum" -ne 0 ]; then                    # Error control
        log "${COL_BOX_RED}ERROR${COL_RESET} ${COL_FG_RED}Errors executing SQL file!${COL_RESET}"
        log "${COL_FG_RED}Failed SQL Script:${COL_RESET} [ ${SQLFILE} ]"
        log "${COL_FG_RED}Failed Script Log File:${COL_RESET} [ ${SQLLOGFILE} ]"
        log "${COL_BOX_RED}INFO${COL_RESET} ${COL_FG_RED}Process ${PROGRAM} cancelled - SQL execution finished with errors, exitting!${COL_RESET}"
        exit 1;
    fi
}

export -f exec_sql

########################################################################################################################
# MAIN Process
########################################################################################################################
init_log

log "${COL_FG_TURQUOISE}------------------------------------------------------------------------------------------------------------------------${COL_RESET}"
log "${COL_FG_TURQUOISE}Creating Onjects on Oracle Database${COL_RESET}"
log ""
log "${COL_FG_TURQUOISE}Hostname:${COL_RESET} [ ${HOST} ]"
log "${COL_FG_TURQUOISE}Program :${COL_RESET} [ ${PROGRAM} ]"
log "${COL_FG_TURQUOISE}Log File:${COL_RESET} [ ${LOGFILE} ]"
log "${COL_FG_TURQUOISE}User:${COL_RESET} [ ${ADM_USR} ]"
log "${COL_FG_TURQUOISE}Oracle SID:${COL_RESET} [ ${ORACLE_SID} ]"
log "${COL_FG_TURQUOISE}------------------------------------------------------------------------------------------------------------------------${COL_RESET}"

# ----------------------------------------------------------------------------------------------------------------------
# CORE DB Objects
# ----------------------------------------------------------------------------------------------------------------------
log "Creating DB Objects..."
exec_sql "ADMIN_core_Total_addon"
exec_sql "ADMIN_Grants"

# ----------------------------------------------------------------------------------------------------------------------
# Final Steps
# ----------------------------------------------------------------------------------------------------------------------
exec_sql "pkg_module_managements"

log ""
log "${COL_BOX_GREEN}INFO${COL_RESET} ${COL_FG_GREEN}Process finished succesfully, exiting!${COL_RESET}"
exit 0
