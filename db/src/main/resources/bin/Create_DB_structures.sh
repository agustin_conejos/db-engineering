#!/bin/bash
########################################################################################################################
# Create_DB_structures.sh
#
# Date      Vers    Description
# --------- ------- ---------------------------------------------------------------------------------------------------
# 20200220  1.0     1st Version
########################################################################################################################

########################################################################################################################
# Program Variables
########################################################################################################################
HOST=$(hostname | tr '[:lower:]' '[:upper:]')
PROGRAM=$(basename "$0" .sh)

# Obtain PATHs Dynamically
APPL_DIR_TEMP=$(dirname "$0")                                               # Script path APPL_DIR_TEMP
CALL_PATH=$(pwd)                                                            # Executing from CALLPATH
if [[ "${APPL_DIR_TEMP}" != /* ]]; then                                     # Define application dir
    APPL_DIR=${CALL_PATH%%/bin}
else
    APPL_DIR=${APPL_DIR_TEMP%%/bin}
fi

APP_BIN_DIR=${APPL_DIR}/bin
APP_CFG_DIR=${APPL_DIR}/cfg
APP_LOG_DIR=${APPL_DIR}/log
APP_SQL_DIR=${APPL_DIR}/sql

. "${APP_CFG_DIR}"/environment                                              # Load environment file

########################################################################################################################
# LOG Functions
########################################################################################################################
init_log() {
    CURRDATE=$(date '+%Y%m%d')
    CURRTIME=$(date '+%H%M%S')
    LOGFILE="${APP_LOG_DIR}"/"${CURRDATE}"_"${CURRTIME}"_"${PROGRAM}".log
}

tty -s && log() {                                                               # if connected to a tty output the log messages otherwise just store them
    echo -e "$*" | tee -a "$LOGFILE"
} ||
log() {
    echo -e "$*" >> "$LOGFILE"
}

########################################################################################################################
# EXEC SQL Function
########################################################################################################################
exec_sql() {
    EXECDATE=$(date '+%Y%m%d')
    EXECTIME=$(date '+%H%M%S')
    FILENAME="$1"
    PARAMS="$2"
    SQLFILE="${APP_SQL_DIR}"/"${FILENAME}".sql
    SQLLOGFILE="${APP_LOG_DIR}"/"${EXECDATE}"_"${EXECTIME}"_"${FILENAME}".sql.log

    if [ ! -f "${SQLFILE}" ]; then                                              # Check that file exists
        log "${COL_FG_RED}File not found: ${COL_RESET} [ ${SQLFILE} ]"
        log "${COL_BOX_RED}ERROR${COL_RESET} ${COL_FG_RED}Process ${PROGRAM} cancelled due to errors!${COL_RESET}"
        exit 1;
    fi

    if [ "$LOG_VERBOSE" -ne 0 ]; then
        log "${COL_BOX_PURPLE}VERBOSE${COL_RESET} => Executing SQL File: [ ${SQLFILE} ]"
        log "${COL_BOX_PURPLE}VERBOSE${COL_RESET} => Log File: [ ${SQLLOGFILE} ]"
    fi

    sqlplus / as sysdba @"${SQLFILE}" ${PARAMS} > "${SQLLOGFILE}"
    OUTPUT_CODE=$?
    errnum=$(grep -cE "ORA-[0-9]{5}|SP2-[0-9]{4}|PLS-[0-9]{5}" "${SQLLOGFILE}")

    if [ "$OUTPUT_CODE" -ne 0 ] || [ "$errnum" -ne 0 ]; then            # Error control
        log "${COL_BOX_RED}ERROR${COL_RESET} ${COL_FG_RED}Errors executing SQL file!${COL_RESET}"
        log "${COL_FG_RED}Failed SQL Script:${COL_RESET} [ ${SQLFILE} ]"
        log "${COL_FG_RED}Failed Script Log File:${COL_RESET} [ ${SQLLOGFILE} ]"
        log "${COL_BOX_RED}INFO${COL_RESET} ${COL_FG_RED}Process ${PROGRAM} cancelled - SQL execution finished with errors, exitting!${COL_RESET}"
        exit 1;
    fi
}

export -f exec_sql

########################################################################################################################
# MAIN Process
########################################################################################################################
init_log

log "${COL_FG_TURQUOISE}------------------------------------------------------------------------------------------------------------------------${COL_RESET}"
log "${COL_FG_TURQUOISE}Creating Structures on Oracle Database${COL_RESET}"
log ""
log "${COL_FG_TURQUOISE}Hostname:${COL_RESET} [ ${HOST} ]"
log "${COL_FG_TURQUOISE}Program :${COL_RESET} [ ${PROGRAM} ]"
log "${COL_FG_TURQUOISE}Log File:${COL_RESET} [ ${LOGFILE} ]"
log "${COL_FG_TURQUOISE}User:${COL_RESET} [ ${ADM_USR} ]"
log "${COL_FG_TURQUOISE}Oracle SID:${COL_RESET} [ ${ORACLE_SID} ]"
log "${COL_FG_TURQUOISE}------------------------------------------------------------------------------------------------------------------------${COL_RESET}"

# 1 DELETE PREVIOUS DB - Ask user confirmation
log "Initial DB Structures will be removed from the system: Tablespaces, Schemas..."
log "Do you wish to drop all PFM Structures?"
log ""
PS4="Choose the option number:"

select validation in 'Drop Structures' 'Skip drop and create new Structures' 'Stop Process'
do
    break
done

log ""
if [ "$validation" = "Stop Process" ]; then                    # Deal with response
    log "${COL_BOX_BLUE}INFO${COL_RESET} ${COL_FG_BLUE}User requested to Stop Process, exiting!${COL_RESET}"
    exit 0
else
    if [ "$validation" = "Drop Structures" ]; then
        log "Deleting previous DB..."
        if [ -z "${ORACLE_PDB}" ]; then
            exec_sql Delete_All "$ADM_USR $APP_USR $APP_ROLE $BO_USR $TBS_NAME"
        else
            exec_sql Delete_All_12 "$ADM_USR $APP_USR $APP_ROLE $BO_USR $TBS_NAME $ORACLE_PDB"
        fi
    fi
fi

# 2 CREATE DB STRUCTURES
log "Creating DB Structures..."
if [ -z "${ORACLE_PDB}" ]; then
    exec_sql AMIN_Structure_Creation "$ADM_USR $ADM_PWD $APP_USR $APP_PWD $APP_ROLE $TBS_NAME $TBS_DATAFILE"
else
    exec_sql Structure_Creation_12 "$ADM_USR $ADM_PWD $APP_USR $APP_PWD $APP_ROLE $TBS_NAME $TBS_DATAFILE $ORACLE_PDB"
fi

log ""
log "${COL_BOX_GREEN}INFO${COL_RESET} ${COL_FG_GREEN}Process finished succesfully, exiting!${COL_RESET}"
exit 0
