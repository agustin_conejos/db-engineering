whenever sqlerror exit 1

set echo on
set verify off

----------------------------
-- MODULE MANAGEMENT PKG  --
----------------------------
create or replace package       module_management
is
---------------------------------------------------------------------------
-- Version: v1.2 03/11/2017_ISG
-- Created by ISG
-- Creation date: 11/04/2014
--
---------------------------------------------------------------------------
function version
return varchar2;

procedure purge_log
(p_days                      number);

procedure privs_syns_to_user
(p_grantee      IN           adm_user_privs.grantee%TYPE
,p_owner        IN           adm_user_privs.owner%TYPE
,p_role         IN           adm_user_privs.role%TYPE
,p_grantable    IN           adm_user_privs.grantable%TYPE
,p_trace        IN           boolean default FALSE);

procedure manage_user_privs_syns
(p_grantee      IN           varchar2
,p_trace        IN           boolean default FALSE);

procedure manage_privs_syns_on_owner
(p_owner        IN           admin.adm_user_privs.owner%TYPE
,p_trace        IN           boolean default FALSE);

procedure manage_all_user_privs_syns
(p_trace        IN           boolean default FALSE);

procedure delete_privs_syns_from_user
(p_grantee      IN           adm_user_privs.grantee%TYPE
,p_owner        IN           adm_user_privs.owner%TYPE
,p_trace        IN           boolean default FALSE);

end module_management;
/

CREATE OR REPLACE PACKAGE BODY module_management IS
/*-------------------------------------------------------------------------
/* Version: v1.2 03/11/2017_ISG*/
/* Created by ISG*/
/* Creation date: 11/04/2014*/
/* Modifications (please: modify version function):*/
/*    v0.1 11/04/2014 ISG - Creation*/
/*    v0.2 08/05/2014 ISG - Add procdure manage_privs_syns_on_owner*/
/*    v1.0 30/05/2014_ISG - Only grant/revoke required privileges*/
/*    v1.1 10/10/2014_ISG - Solve problem with user with privs on more than one schema*/
/*    v1.2 03/11/2017_ISG - Add procedure delete_privs_syns_to_user
/*	  v1.3 04/03/2020_AAC - Checks on VALID objects
/**/
/*-----------------------------------------------------------------------*/

/* global constants*/

	c_version          CONSTANT VARCHAR2(20) := 'v1.3 04/03/2020_AAC';
	c_role_rw          CONSTANT VARCHAR2(2) := 'RW';
	c_role_ro          CONSTANT VARCHAR2(2) := 'RO';
	c_log_error_type   CONSTANT log_module_management.log_type%TYPE := 'ERROR';
	c_log_trace_type   CONSTANT log_module_management.log_type%TYPE := 'TRACE';

/* global variables*/
	v_trace            BOOLEAN;

/* private functions/procedures*/

	PROCEDURE log_text (
		p_type   log_module_management.log_type%TYPE
		, p_text   log_module_management.log_text%TYPE
	) IS
		PRAGMA autonomous_transaction;
	BEGIN
		INSERT INTO log_module_management (
			log_seq
			, log_date
			, log_type
			, log_text
		) VALUES (
			log_module_management_seq.NEXTVAL
			, sysdate
			, p_type
			, p_text
		);

		COMMIT;
	END log_text;

/* private functions/procedures*/

	PROCEDURE log_message (
		p_message CLOB
	) IS
		v_amount   NUMBER(5);
		v_offset   NUMBER(5);
		v_buffer   VARCHAR2(250);
	BEGIN
		log_text(c_log_error_type, p_message);
		v_amount := 250;
		v_offset := 1;
		WHILE v_offset < dbms_lob.getlength(p_message) LOOP
			dbms_lob.read(p_message, v_amount, v_offset, v_buffer);
			dbms_output.put_line(v_buffer);
			v_offset := v_offset + v_amount;
		END LOOP;

	END log_message;

	PROCEDURE trace_message (
		p_message CLOB
	) IS
	BEGIN
		log_text(c_log_trace_type, p_message);
	END trace_message;

	PROCEDURE execute_sql (
		p_sql    CLOB
		, p_wait   BOOLEAN DEFAULT false
	) IS
	BEGIN
		trace_message(p_sql);
		IF NOT v_trace THEN
			EXECUTE IMMEDIATE p_sql;
			IF p_wait THEN dbms_lock.sleep(10);
			END IF;
		END IF;

	END execute_sql;

/* Validate if the grantee is a role or a user*/

	FUNCTION grantee_is_role (
		p_grantee IN VARCHAR2
	) RETURN BOOLEAN IS
		r_roles dba_roles%rowtype;
	BEGIN
		SELECT *
		  INTO r_roles
		  FROM dba_roles
		 WHERE role = p_grantee;

		RETURN true;
	EXCEPTION
		WHEN OTHERS THEN RETURN false;
	END grantee_is_role;

/* public functions/procedures*/

	FUNCTION version RETURN VARCHAR2 IS
	BEGIN
		RETURN c_version;
	END version;

	PROCEDURE purge_log (
		p_days NUMBER
	) IS
		v_module   VARCHAR2(30);
		v_count    NUMBER(5);
	BEGIN
		v_module := 'purge_log';
		DELETE FROM log_module_management
		 WHERE log_date <= sysdate - p_days;

		v_count := SQL%rowcount;
		COMMIT;
		trace_message('<' || v_module || '> Version : ' || version || ' Initial values: ' || p_days || '. Records deleted: ' || v_count
		);

	EXCEPTION
		WHEN OTHERS THEN log_message('<' || v_module || '> ERROR: ' || sqlerrm);
	END purge_log;

	PROCEDURE privs_syns_to_user (
		p_grantee     IN   adm_user_privs.grantee%TYPE
		, p_owner       IN   adm_user_privs.owner%TYPE
		, p_role        IN   adm_user_privs.role%TYPE
		, p_grantable   IN   adm_user_privs.grantable%TYPE
		, p_trace       IN   BOOLEAN DEFAULT false
	) IS

		v_module        VARCHAR2(30);
		v_grant_privs   VARCHAR2(2000);
		v_sql           CLOB;
		v_grantable     VARCHAR2(20);
		v_error         BOOLEAN;
		v_is_role       BOOLEAN;
		var_rows        NUMBER := 0;
	BEGIN
		v_trace := p_trace;
		v_module := 'privs_syns_to_user';
		trace_message('<' || v_module || '> Version : ' || version || ' Initial values: ' || p_grantee || ' ' || p_owner || ' ' || p_role
		|| ' ' || p_grantable);

		v_is_role := grantee_is_role(p_grantee);
		IF p_grantable = 'Y' THEN v_grantable := ' with grant option';
		ELSE v_grantable := '';
		END IF;

/* delete unnecessary synonyms*/

		FOR c_del_syns IN (
			SELECT s.table_name name
			  FROM dba_synonyms s
			 WHERE s.owner = p_grantee
			   AND s.table_owner = p_owner
			MINUS
			SELECT o.object_name name
			  FROM dba_objects o
			 WHERE o.owner = p_owner
			   AND NOT ( o.object_name LIKE 'SYS_IOT_OVER_%'
			   AND o.generated = 'Y' )
		) LOOP
			v_sql := 'drop synonym ' || p_grantee || '.' || c_del_syns.name;
			BEGIN
				execute_sql(v_sql);
				var_rows := 1 + var_rows;
			EXCEPTION
				WHEN OTHERS THEN log_message('<' || v_module || '> ERROR: ' || sqlerrm);
			END;

		END LOOP;

/* Log results and restart counter*/

		IF var_rows != 0 THEN
			trace_message('<' || v_module || '> INFO: ' || var_rows || ' number of deleted synonyms');
			var_rows := 0;
		END IF;

/* revoke unnecessary privileges*/
/* this changes imply longer execution times as this takes 1 seg per extra per loop*/

		FOR c_revoke_privs IN (
			SELECT p.table_name   name
			       , p.privilege    priv
			       , p.grantable
			  FROM dba_tab_privs   p
			       , dba_objects     o
			 WHERE p.grantee = p_grantee
			   AND o.owner = p_owner
			   AND o.owner = p.owner
			   AND p.table_name = o.object_name
			   AND o.status = 'VALID'
			   AND p.table_name NOT LIKE 'BIN$%'
			MINUS
			SELECT o.object_name   name
			       , p.privilege     priv
			       , decode(p_grantable, 'N', 'NO', 'YES') grantable
			  FROM dba_objects             o
			       , cfg_object_type_privs   p
			 WHERE o.owner = p_owner
			   AND NOT ( o.object_name LIKE 'SYS_IOT_OVER_%'
			   AND o.generated = 'Y' )
			   AND o.object_type = p.object_type
			   AND p.role = p_role
		) LOOP
			v_sql := 'revoke ' || c_revoke_privs.priv || ' on ' || p_owner || '.' || c_revoke_privs.name || ' from ' || p_grantee;
/* execute the grant, but, in case of error, register in the log and continue.*/

			BEGIN
				execute_sql(v_sql);
				var_rows := 1 + var_rows;
			EXCEPTION
				WHEN OTHERS THEN
					log_message('<' || v_module || '> REVOKE ERROR: ' || sqlerrm);
					v_error := true;
			END;

		END LOOP;
/* Log results and restart counter*/

		IF var_rows != 0 THEN
			trace_message('<' || v_module || '> INFO: ' || var_rows || ' unnecesary privs revoked');
			var_rows := 0;
		END IF;

/* grant new privileges*/

		FOR c_grant_privs IN (
			SELECT o.object_name   name
			       , p.privilege     priv
			       , decode(p_grantable, 'N', 'NO', 'YES') grantable
			  FROM dba_objects             o
			       , cfg_object_type_privs   p
			 WHERE o.owner = p_owner
			   AND o.status = 'VALID'
			   AND NOT ( o.object_name LIKE 'SYS_IOT_OVER_%'
			   AND o.generated = 'Y' )
			   AND o.object_type = p.object_type
			   AND p.role = p_role
			MINUS
			SELECT table_name   name
			       , privilege    priv
			       , grantable
			  FROM dba_tab_privs
			 WHERE grantee = p_grantee
			   AND owner = p_owner
		) LOOP
			v_sql := 'grant ' || c_grant_privs.priv || ' on ' || p_owner || '.' || c_grant_privs.name || ' to ' || p_grantee || v_grantable
			;

			BEGIN
				execute_sql(v_sql);
				var_rows := 1 + var_rows;
			EXCEPTION
				WHEN OTHERS THEN log_message('<' || v_module || '> ERROR: ' || sqlerrm);
			END;

		END LOOP;

/* Log results and restart counter*/

		IF var_rows != 0 THEN
			trace_message('<' || v_module || '> INFO: ' || var_rows - 1 || ' new privs granted');

			var_rows := 0;
		END IF;

/* create new synonyms*/

		IF v_is_role = false THEN
			FOR c_cre_syns IN (
				SELECT o.object_name name
				  FROM dba_objects o
				 WHERE o.owner = p_owner
				   AND NOT ( o.object_name LIKE 'SYS_IOT_OVER_%'
				   AND o.generated = 'Y' )
				   AND o.status = 'VALID'
				   AND o.object_type IN (
					'TABLE'
					, 'VIEW'
					, 'SEQUENCE'
					, 'JAVA CLASS'
					, 'PACKAGE'
					, 'FUNCTION'
					, 'PROCEDURE'
					, 'MATERIALIZED VIEW'
					, 'SYNONYM'
				)
				MINUS
				SELECT s.table_name name
				  FROM dba_synonyms s
				 WHERE s.owner = p_grantee
			) LOOP
				v_sql := 'create or replace synonym ' || p_grantee || '.' || c_cre_syns.name || ' for ' || p_owner || '.' || c_cre_syns.name;

				BEGIN
					execute_sql(v_sql);
					var_rows := 1 + var_rows;
				EXCEPTION
					WHEN OTHERS THEN log_message('<' || v_module || '> ERROR: ' || sqlerrm);
				END;

			END LOOP;
/* Log results and restart counter*/

			IF var_rows != 0 THEN
				trace_message('<' || v_module || '> INFO: ' || var_rows || ' new synonyms created');
				var_rows := 0;
			END IF;

		END IF;

		trace_message('<' || v_module || '> End');
	EXCEPTION
		WHEN OTHERS THEN log_message('<' || v_module || '> ERROR: ' || sqlerrm);
	END privs_syns_to_user;

	PROCEDURE manage_user_privs_syns (
		p_grantee   IN   VARCHAR2
		, p_trace     IN   BOOLEAN DEFAULT false
	) IS
		v_module VARCHAR2(30);
	BEGIN
		v_trace := p_trace;
		v_module := 'manage_user_privs_syns';
		trace_message('<' || v_module || '> Version : ' || version || ' Initial values: ' || p_grantee);

/* get privileges on other schemas*/

		FOR c_privs IN (
			SELECT owner
			       , role
			       , grantable
			  FROM admin.adm_user_privs
			 WHERE grantee = p_grantee
		) LOOP privs_syns_to_user(p_grantee => p_grantee, p_owner => c_privs.owner, p_role => c_privs.role, p_grantable => c_privs.grantable
		, p_trace => v_trace);
		END LOOP;

		trace_message('<' || v_module || '> End');
	EXCEPTION
		WHEN OTHERS THEN log_message('<' || v_module || '> ERROR: ' || sqlerrm);
	END manage_user_privs_syns;

	PROCEDURE manage_privs_syns_on_owner (
		p_owner   IN   admin.adm_user_privs.owner%TYPE
		, p_trace   IN   BOOLEAN DEFAULT false
	) IS
		v_module VARCHAR2(30);
	BEGIN
		v_trace := p_trace;
		v_module := 'manage_privs_syns_on_owner';
		trace_message('<' || v_module || '> Version : ' || version || ' Initial values: ' || p_owner);

/* get privileges on other schemas*/

		FOR c_privs IN (
			SELECT grantee
			       , role
			       , grantable
			  FROM admin.adm_user_privs
			 WHERE owner = p_owner
		) LOOP privs_syns_to_user(p_grantee => c_privs.grantee, p_owner => p_owner, p_role => c_privs.role, p_grantable => c_privs.grantable
		, p_trace => v_trace);
		END LOOP;

		trace_message('<' || v_module || '> End');
	EXCEPTION
		WHEN OTHERS THEN log_message('<' || v_module || '> ERROR: ' || sqlerrm);
	END manage_privs_syns_on_owner;

	PROCEDURE manage_all_user_privs_syns (
		p_trace IN BOOLEAN DEFAULT false
	) IS
		v_module VARCHAR2(30);
	BEGIN
		v_trace := p_trace;
		v_module := 'manage_all_user_privs_syns';
		trace_message('<' || v_module || '> Version : ' || version);

/* get privileges for all users*/
		FOR c_privs IN (
			SELECT grantee
			       , owner
			       , role
			       , grantable
			  FROM admin.adm_user_privs
			 ORDER BY position
			          , grantee
			          , owner
		) LOOP privs_syns_to_user(p_grantee => c_privs.grantee, p_owner => c_privs.owner, p_role => c_privs.role, p_grantable => c_privs
		.grantable, p_trace => v_trace);
		END LOOP;

		trace_message('<' || v_module || '> End');
	EXCEPTION
		WHEN OTHERS THEN log_message('<' || v_module || '> ERROR: ' || sqlerrm);
	END manage_all_user_privs_syns;

	PROCEDURE delete_privs_syns_from_user (
		p_grantee   IN   adm_user_privs.grantee%TYPE
		, p_owner     IN   adm_user_privs.owner%TYPE
		, p_trace     IN   BOOLEAN DEFAULT false
	) IS
		v_module   VARCHAR2(30);
		v_sql      CLOB;
		v_count    NUMBER;
		var_rows   NUMBER;
	BEGIN
		v_trace := p_trace;
		v_module := 'delete_privs_syns_from_user';
		trace_message('<' || v_module || '> Version : ' || version || ' Initial values: ' || p_grantee || ' ' || p_owner);

/* Validate that the user is configured*/

		SELECT COUNT(1)
		  INTO v_count
		  FROM admin.adm_user_privs
		 WHERE grantee = p_grantee
		   AND owner = p_owner;

		IF v_count = 1 THEN

/* delete configuration from adm_user_privs*/
			trace_message('delete from admin.adm_user_privs where grantee = ' || p_grantee || ' and owner = ' || p_owner);
			IF NOT v_trace THEN DELETE FROM admin.adm_user_privs
			 WHERE grantee = p_grantee
			   AND owner = p_owner;

			END IF;

/* revoke unnecessary privileges*/

			FOR c_revoke_privs IN (
				SELECT p.table_name   name
				       , p.privilege    priv
				  FROM dba_tab_privs   p
				       , dba_objects     o
				 WHERE p.grantee = p_grantee
				   AND p.owner = p_owner
				   AND o.object_name = p.table_name
				   AND o.status = 'VALID'
				   AND p.table_name NOT LIKE 'BIN$%'
			) LOOP
				v_sql := 'revoke ' || c_revoke_privs.priv || ' on ' || p_owner || '.' || c_revoke_privs.name || ' from ' || p_grantee;

				BEGIN
					execute_sql(v_sql);
					var_rows := 1 + var_rows;
				EXCEPTION
					WHEN OTHERS THEN log_message('<' || v_module || '> ERROR: ' || sqlerrm);
				END;

			END LOOP;

			trace_message('<' || v_module || '> INFO: ' || var_rows || ' revoke unnecessary privileges');
			var_rows := 0;
/* delete unnecessary synonyms*/
			FOR c_del_syns IN (
				SELECT s.table_name name
				  FROM dba_synonyms s
				 WHERE s.owner = p_grantee
				   AND s.table_owner = p_owner
			) LOOP
				v_sql := 'drop synonym ' || p_grantee || '.' || c_del_syns.name;
				BEGIN
					execute_sql(v_sql);
					var_rows := 1 + var_rows;
				EXCEPTION
					WHEN OTHERS THEN log_message('<' || v_module || '> ERROR: ' || sqlerrm);
				END;

			END LOOP;

			trace_message('<' || v_module || '> INFO: ' || var_rows || ' revoke unnecessary privileges');
			var_rows := 0;
		ELSE log_message('Configuration not found in adm_user_privs');
		END IF;

		trace_message('<' || v_module || '> End');
	EXCEPTION
		WHEN OTHERS THEN log_message('<' || v_module || '> ERROR: ' || sqlerrm);
	END delete_privs_syns_from_user;

END module_management;
/

exit
